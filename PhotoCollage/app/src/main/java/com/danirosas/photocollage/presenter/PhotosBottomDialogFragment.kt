package com.danirosas.photocollage.presenter

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.danirosas.photocollage.*
import com.danirosas.photocollage.domain.Photo
import com.danirosas.photocollage.domain.PhotoStore
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.layout_photo_bottom_sheet.*


class PhotosBottomDialogFragment : BottomSheetDialogFragment(),
    PhotosAdapter.PhotoListener {

    private lateinit var viewModel: SharedViewModel

    private val selectedPhotosSubject = PublishSubject.create<Photo>()

    val selectedPhotos: Observable<Photo>
        get() = selectedPhotosSubject.hide()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_photo_bottom_sheet, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val ctx = activity
        ctx?.let {
            viewModel = ViewModelProviders.of(ctx).get(SharedViewModel::class.java)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        photosRecyclerView.layoutManager = GridLayoutManager(context, 3)
        photosRecyclerView.adapter =
            PhotosAdapter(
                PhotoStore.photos,
                this
            )
    }

    override fun onDestroy() {
        selectedPhotosSubject.onComplete()
        super.onDestroy()
    }

    override fun photoClicked(photo: Photo) {
        selectedPhotosSubject.onNext(photo)
    }

    companion object {
        fun newInstance(): PhotosBottomDialogFragment {
            return PhotosBottomDialogFragment()
        }
    }
}