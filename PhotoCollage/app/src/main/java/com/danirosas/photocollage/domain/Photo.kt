package com.danirosas.photocollage.domain

data class Photo(val drawable: Int)