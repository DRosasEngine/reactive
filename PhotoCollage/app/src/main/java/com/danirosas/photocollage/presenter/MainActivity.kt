package com.danirosas.photocollage.presenter

import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.danirosas.photocollage.R
import com.danirosas.photocollage.domain.Photo
import com.danirosas.photocollage.domain.PhotoStore
import com.danirosas.photocollage.utils.combineImages
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.list_item_photo.*

class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: SharedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        title = getString(R.string.collage)

        viewModel = ViewModelProviders.of(this).get(SharedViewModel::class.java)

        setListeners()

        viewModel.selectedPhotos.observe(this, Observer { photos ->
            if(photos.isNotEmpty()){
                val bitmaps = photos.map {
                    BitmapFactory.decodeResource(resources, it.drawable)
                }
                val newBitmap = combineImages(bitmaps)
                collageImage.setImageDrawable(BitmapDrawable(resources, newBitmap))
            } else {
                collageImage.setImageResource(android.R.color.transparent)
            }

            updateUi(photos)
        })
    }

    private fun updateUi(photos: List<Photo>) {
        saveButton.isEnabled = photos.isNotEmpty() && (photos.size % 2 == 0)
        clearButton.isEnabled = photos.isNotEmpty()
        addButton.isEnabled = photos.size < 6
        title = if(photos.isNotEmpty()){
            resources.getQuantityString(R.plurals.photos_format, photos.size)
        }else{
            getString(R.string.collage)
        }
    }

    private fun setListeners() {
        addButton.setOnClickListener {
            actionAdd()
        }

        clearButton.setOnClickListener {
            actionClear()
        }

        saveButton.setOnClickListener {
            actionSave()
        }
    }

    private fun actionAdd() {
        val addPhotosBottomDialogFragment = PhotosBottomDialogFragment.newInstance()
        addPhotosBottomDialogFragment.show(supportFragmentManager, "PhotosBttomDialogFragment")

        viewModel.subscribeSelectedPhotos(addPhotosBottomDialogFragment.selectedPhotos)
    }

    private fun actionClear() {
        viewModel.clearPhotos()
    }

    private fun actionSave() {
        viewModel.saveBitmapFromImageView(collageImage, this)
            .subscribeOn(Schedulers.io()) //Instruct the Single to do its subscription work on the IO scheduler.
            .observeOn(AndroidSchedulers.mainThread()) // Instruct the Single to run the subscribeBy code on the Android main thread.
            .subscribeBy (
                onSuccess = {file ->
                    Toast.makeText(this, "$file saved", Toast.LENGTH_SHORT).show()
                },
                onError = { e ->
                    Toast.makeText(this, "Error saving file :${e.localizedMessage}",
                    Toast.LENGTH_LONG).show()
                }
            )
    }
}
