# Reactive Programming

## RxJava Foundations

RxJava finds the sweet spot between traditionally imperative Java/Kotlin code and purist functional code. It allows you to react to events by using immutable code definitions to asynchronously process pieces of input in a deterministic, composable way.
Here I'm going to treat the cornerstone concepts of RxJava development. The 3 main concepts of Rx are **observables**, **operators** and **schedulers**

### Observables
*Observable<T>* class allows one or more observers to subscribe for values emitted by another class over time. Allows to react to any events in real timee and update the app UI or process incoming data.
An *Observable* can emit only three typese of events:
- **next**:  Events which carries the elatest data value. Is how the observers data.
- **complete**: Terminate the events sequence with success and no longer emit any other events.
- **error**: The *Observable* terminates with an error and will not emit other events

The observable contracts does not make any assumptions about the naturee eof the *Observable* or the *Observer*, so using event sequences is the ultimate decoupling practice.

### Operators
*ObservableSource<T>* and the implementation of the *Observable* class include plenty of methods that abstract discrete pieces of asynchronous work, which can be composed together to implement more complex logic.

### Schedulers
You can think of them as a collection of *Threads* that are all joined together and available use.
Most of ViewModel classes don't expose an observable. Instead, they subscribe to those observables internally and expose LiveData objects that work better wuth the Android Lifecycle.

## Concepts
### Observables
Are the heart of Rx. Basically is just a sequence with special powers. One of them, is that is asynchronous. Produce events (emit) over a period of time, which can contain values (numbers, instances of a type) or recognize gesture (taps, swipes, etc).
The best ways to conceptualizee *Observables* is by using marble diagramas

 **--(1)---(2)-(3)-->** 

 The left-to-right arrow represents time, and the numbered circles represent elements of a sequence. Element 1 is emitted, pass some time, and element 2 and 3 are emitted.

 Recapituling. An observable emits **next** events, until an **error** or **complete** event is emitted. Once it is terminated, it can no longer emit events.

 ### Subscribing to observables

 An observable, will not send events until it has a subscriber. To do it, is used the *suscribe()* and *suscribedBy()* methods. 

 ### Disposing and terminatting

 An observable doesn't do anything until it receives a subscription. It's the subscription that triggers an observable to begin emit events, up unitl emtis an error or completed event and is terminated. Also you can manually cause an observable to terminate by cancelling or **disposing** a subscription to it, also known as **Disposable**. To manage it, RxJava offers a *CompositeDisposable* tyoe that holds disposables and will caall *dispose()* on all of them when you call *dispose()* on the *CompositeDisposble* itself.

 ### Create operator
 Is another way to specify all events that an observable will emit to subscribers.

 ```sh

exampleOf("create") {
  Observable.create<String> { emitter ->
    emitter.onNext("1")
    emitter.onNext("?")
  }.subscribeBy(
      onNext = { println(it) },
      onComplete = { println("Completed") },
      onError = { println("Error") }
  )
}

 ```


### Creating observable factories

It's possible to create observable factories that avend a new observable to each subscriber.  For example

```sh
exampleOf("defer") {

  val disposables = CompositeDisposable()
  // 1
  var flip = false
  // 2
  val factory: Observable<Int> = Observable.defer {
    // 3
    flip = !flip
    // 4
    if (flip) {
      Observable.just(1, 2, 3)
    } else {
      Observable.just(4, 5, 6)
    }
  }
}

for (i in 0..3) {
  disposables.add(
      factory.subscribe {
        println(it)
      }
  )
}

disposables.dispose()

```

The result is goint to be: [1,2,3,4,5,6,1,2,3,4,5,6]

### Other observable types

**Single**: Emit a *success* (combination of next and completed) or *error* event. Useful for one-time processes that will either succeed and yield a value or fail. Useful saving a file, downloading a file, loading data from disk, or basically **any asynchronous operation that yields a value**.

**Completable**: Only emit a *completed* or *error* event. Doesn't emit any value. For example, when you app auto-saves a doc while the user is working on it. You'd like to asynchronously save the doc in a background queue, and when completed, show a small noti if the operation fails.

**Maybe**: Mash-up of *Single* and *Completable*. Emite a *success*, *completed* and *error*. Imagine this use case. Your app is storing phtos in its own custom photo album. You persist the album identifier in Shared Preferences and use that ID each time to open the album and write a photo inside. When you design an *open(albumId): Maybe<String>** method to handle the following situations: 
  - Album with given ID stil exists: emit *completed* event
  - User deleted the album, create a new one and emite a *next* event with the new ID and persist it in SharedPreferences.
  - Something went wrong and you can't access the Photos library: emite an *error*


## Subjects

*PublishSubject* is a class that can act as *Observer* anda *Observable*, so cna receive events and be subscribed to. The subject received +next* events. and each time it received an event, it turned around and emitted it to its subscriber. There are 4 types:
- **PublishSubject**: Starts empty and only emits new elements to subscribers.
- **BehaviourSubject**: Starts with an optional initial value and replays it to new subscribers.
- **ReplaySubject**: Initialized with a buffer size and will mantain a buffer of elements up to that size anad replay it to new subscribers.
- **AsyncSubject**: Starts empty and only emits the last item received before it's completede to subscribers.

You might user a publish subject when you're modeling time-sensitive data, such as in an online bidding, or contests app. 

# Testing RxJava

Every RxJava type exposes a *test()* method that returns a *TestObserver*. You can use this class to assert against different conditions on your observable (or whatever other RxJava type you are using).

The TestObserver class that the *test()* method returns has many uses. One of the most convenient is an insight into what values the observer has received so far. You can use the *values()* method on it to get a list of all of the items that observable has emitted. You can assert that your observable has completed, has emitted a few values, has thrown an error...

In order    to tet classes that don't expose their internal observables, you should use de DI design patters to inject the schedulers.

RxLibrary exposes a special scheduler that you can use to control when your observables emite items: *TestScheduler* (synchronous scheduler that allows you to trigger new actions), *TrampolineScheduler* (to make our observables synchronous).

Subjects can be used to precisely control when elements are emitted and, combined with moked data, make for great testing tools.




# DEMO APPS

## PhotoCollage

- Whenever the user taps the Add button, the *MainActivity* class is calling through to the *SharedViewModel* add photo with single static photo.
- The *SharedViewModel* class then updates a list of photos that is stored in *imagesSubject*, and it calls onNext with the updated list of photos.
- Since the ViewModel is subscribed to the *imagesSubject*, it receives the onNext notification and forwards the new list of photos through to the selectedPhotos live data.
- Since the *MainActivity* class is subscribing to the *selectedPhotos* live data, it's notified of the new list of photos. It then create the combined bitmap of photos and sets it on the *collageItems* image view. If the list of photos is empty, it instead clears the image view.

