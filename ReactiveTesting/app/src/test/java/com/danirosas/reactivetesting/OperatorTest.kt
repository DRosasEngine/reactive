package com.danirosas.reactivetesting

import io.reactivex.Observable
import io.reactivex.internal.schedulers.TrampolineScheduler
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Test
import java.util.concurrent.TimeUnit

class OperatorTest {

    @Test
    fun `test concat`() {
        val observableA = Observable.just(1)
        val observableB = Observable.just(2)
        val observableC = observableA.concatWith(observableB)

        observableC.test()
            .assertResult(1,2)
            .assertComplete()
    }

    @Test
    fun `test amb`(){

        val scheduler = TestScheduler()
        /*
        interval factory method creates a new observable that starts counting up at a frequency that you dictate.
        For observableA, the interval is once every second.
        For observableB, the interval is oncee every 500 milliseconds.
        You are taking the first three results from each one and doing math on it.
         */
        val observableA = Observable.interval(1, TimeUnit.SECONDS, scheduler)
            .take(3)
            .map { 5 * it }

        val observableB = Observable.interval(500, TimeUnit.MILLISECONDS, scheduler)
            .take(3)
            .map { 10*it }

        /*
        Creating a new ambObservable using ambWith, which take tow or more observables and combine them
        into a resulting observable that mirrors the first observable to fire. The other one is discarded.
        amb comes in very handy when you have two sources of information and you only care about whichever
        one is the fastest. Imagine reading from database and pulling from database.
         */
        val ambObservable = observableA.ambWith(observableB)

        val testObserver = ambObservable.test()

        /*
         * asserValueCount asserts that the observable has emitted a certaing number of values.
         */
        scheduler.advanceTimeBy(500, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(1)

        scheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(3)
        testObserver.assertResult(0L, 10L, 20L)
        testObserver.assertComplete()
    }

    @Test
    fun `using trampoline schedulers`(){

        //It will be run on whatever the current thread is, thus blocking the method until it finishes.
        val observableA = Observable.just(1)
            .subscribeOn(TrampolineScheduler.instance())

        //It will run on a different thread and the method would terminate before finished
        val observableB = Observable.just(1)
            .subscribeOn(Schedulers.io())

        observableA.test().assertResult(1)
        observableB.test().assertEmpty()
    }
}