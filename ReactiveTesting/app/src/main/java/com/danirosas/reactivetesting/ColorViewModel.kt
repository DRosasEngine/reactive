package com.danirosas.reactivetesting

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject

class ColorViewModel(backgroundScheduler: Scheduler,
                     mainScheduler: Scheduler,
                     colorCoordinator: ColorCoordinator) : ViewModel() {

    val hexStringSubject = BehaviorSubject.createDefault("#")
    val hexStringLiveData = MutableLiveData<String>()
    val backgroundColorLiveData = MutableLiveData<Int>()
    val rgbStringLiveData = MutableLiveData<String>()
    val colorNameLiveData = MutableLiveData<String>()

    private val disposables = CompositeDisposable()

    init {

        // Send the hex string to the activity
        hexStringSubject
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .subscribe(hexStringLiveData::postValue)
            .addTo(disposables)

        // Send the actual color object to the activity
        hexStringSubject
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .map { if (it.length < 7) "#000000" else it }
            .map { colorCoordinator.parseColor(it) }
            .subscribe(backgroundColorLiveData::postValue)
            .addTo(disposables)

        // Send over the color name "--" if the hex string is less than seven chars
        hexStringSubject
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .filter { it.length < 7 }
            .map { "--" }
            .subscribe(colorNameLiveData::postValue)
            .addTo(disposables)

        // If our color name enum contains the given hex string, send that color name over.
        hexStringSubject
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .filter { hexString ->
                ColorName.values().map { it.hex }.contains(hexString)
            }
            .map { hexString ->
                ColorName.values().first { it.hex == hexString }
            }
            .map { it.toString() }
            .subscribe(colorNameLiveData::postValue)
            .addTo(disposables)

        // Send the RGB values of the color to the activity.
        hexStringSubject
            .subscribeOn(backgroundScheduler)
            .observeOn(mainScheduler)
            .map {
                if (it.length == 7) {
                    colorCoordinator.parseRgbColor(it)
                } else {
                    RGBColor(0, 0, 0)
                }
            }
            .map { "${it.red},${it.green},${it.blue}" }
            .subscribe(rgbStringLiveData::postValue)
            .addTo(disposables)
    }

    private fun currentHexValue(): String {
        return hexStringSubject.value ?: ""
    }

    fun backClicked() {
        if (currentHexValue().length >= 2) {
            hexStringSubject.onNext(currentHexValue().substring(0, currentHexValue().lastIndex))
        }
    }

    fun clearClicked() {
        hexStringSubject.onNext("#")
    }

    fun digitClicked(digit: String) {
        if (currentHexValue().length < 7) {
            hexStringSubject.onNext(currentHexValue() + digit)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}