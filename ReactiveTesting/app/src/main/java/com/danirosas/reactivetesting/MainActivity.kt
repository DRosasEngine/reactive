package com.danirosas.reactivetesting

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.graphics.drawable.ColorDrawable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.text.clear

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val viewModel = ViewModelProviders.of(this, object : ViewModelProvider.NewInstanceFactory() {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return ColorViewModel(Schedulers.io(), AndroidSchedulers.mainThread(), ColorCoordinator()) as T
            }
        }).get(ColorViewModel::class.java)

        val digits = listOf(zero, one, two, three, four, five, six, seven, eight, nine, A, B, C, D, E, F)

        digits.forEach { textView ->
            textView.setOnClickListener { viewModel.digitClicked(textView.text.toString()) }
        }

        clear.setOnClickListener {
            viewModel.clearClicked()
        }

        back.setOnClickListener {
            viewModel.backClicked()
        }

        viewModel.hexStringLiveData.observe(this, Observer {
            hex.text = it
        })

        viewModel.rgbStringLiveData.observe(this, Observer {
            rgb.text = it
        })
        viewModel.colorNameLiveData.observe(this, Observer {
            color_name.text = it
        })
        viewModel.backgroundColorLiveData.observe(this, Observer {
            animateColorChange(it)
        })
    }

    private fun animateColorChange(newColor: Int) {
        val colorFrom = root_layout.background as ColorDrawable
        val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom.color, newColor)
        colorAnimation.duration = 250 // milliseconds
        colorAnimation.addUpdateListener { animator ->
            val color = animator.animatedValue as Int
            root_layout.setBackgroundColor(color)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.statusBarColor = color
            }
        }
        colorAnimation.start()
    }
}
